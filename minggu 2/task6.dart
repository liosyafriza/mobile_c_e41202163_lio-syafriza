import 'dart:io';

void main() {
  stdout.write('MASUKKAN NAMA : ');
  var name = stdin.readLineSync();

  stdout.write('MASUKKAN PERAN : ');
  var peran = stdin.readLineSync();

  if (name == '') {
    print('harus di isi');
  } else if (peran == '') {
    print('hallo $name, pilih peran anda untuk memulai permainan ');
  } else if (peran == 'penyihir') {
    print(
        'selamat datang di permainan werewolf, $name\nHalo $peran $name, kamu dapat melihat siapa yang menjadi werewolf');
  } else if (peran == 'guard') {
    print(
        'selamat datang di permainan werewolf, $name\nHalo $peran $name, kamu akan membantu melindungi temanmu dari serangan werewolf');
  } else if (peran == 'werewolf') {
    print(
        'selamat datang di permainan werewolf, $name\nHalo $peran $name, kamu akan memakan mangsa setiap malam');
  } else {
    print('error!');
  }
}

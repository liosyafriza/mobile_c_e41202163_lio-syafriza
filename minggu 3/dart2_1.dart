void main() {
  hello();
  print('----------------------------------------');
  hello2('LIO SYAFRIZA', 8);
}

void hello() {
  print('Hallo, LIO !');
}

void hello2(String nama, int hari) {
  print('Hallo, $nama!');
  print('Anda sedang belajar di pertemuan ke $hari.');
}

// Arrow Function
void hello3(String nama, int hari) => // '=>' mendefinisikan return
    '\n Anda sedang belajar di pertemuan ke $hari.';
